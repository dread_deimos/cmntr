
function addComment(event) {
	event.preventDefault();
	$.ajax('/comment', {
		data: { 
			author:  prompt('Author name:'), 
			text:    prompt('Comment text:'),
			parent:  $(event.target).data('id'),
			post_id: $(event.target).closest('article').data('post-id')
		},
		type:     'POST',
		dataType: 'JSON'
	}).success(function(data){
		getComments($(event.target).closest('article'));
	});
}

function deleteComment(event) {
	event.preventDefault();
	$.ajax('/comment', {
		data:     { id: $(event.target).data('id') },
		type:     'DELETE',
		dataType: 'JSON'
	}).success(function(data){
		if (data.status == 1) {
			$(event.target).closest('.comment').remove();
		}
	});
}

function renderComments(element, data) {
	$.each(data, function(i, item) {
		var body = $(
			'<div class="comment"><b>' 
			+ item.author 
			+ '</b> @ ' 
			+ item.timestamp 
			+ '<p>' 
			+ item.text 
			+ '</p></div>'
		);
		var controls = $('<p/>')
			.append( $('<a>reply</a>').data('id', item.id).click(addComment) )
			.append( $('<a>delete</a>').data('id', item.id).click(deleteComment) )
		;
		body.append(controls);
		element.append(body);
		if (item.replies) {
			renderComments(body, item.replies);
		}
	});
}

function getComments(article) {
	$.ajax('/comments', {
		type:     'GET',
		dataType: 'JSON',
		data:     { post_id: article.data('post-id') }
	}).success(function(data){
		renderComments(
			article.find('.comment-container').html(''), data
		);
	});
}

$(document).ready(function(){

	$('article').each(function(i, article){
		$(article).find('[href=#refresh]').click(function(e){
			e.preventDefault();
			getComments($(article));
		}).click();
		$('[href=#add-comment]').click(addComment);
	});

});