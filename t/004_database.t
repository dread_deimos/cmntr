use Test::More tests => 7;

use lib 'lib';
use strict;
use warnings;

use_ok 'Database';

my $db = Database->new;

my $POST_ID = 100;

is(ref $db->comments($POST_ID), 'ARRAY', 'empty list');

my $id = $db->add_comment($POST_ID, 'test', 'tost');

ok(scalar @{ $db->comments($POST_ID) } == 1 && $id == 1, 'add first comment');

my $comment = $db->get_comment($id);

ok(
	$comment->{id} == $id
	&& $comment->{post_id} == $POST_ID
	&& $comment->{author} eq 'test'
	&& $comment->{text} eq 'tost'
	&& $comment->{timestamp} > 0,
	'get comment')
;

my $child_id = $db->add_comment($POST_ID, 'tast', 'tust', $id);
my $reply = $db->get_comment($child_id);

ok(
	$reply->{id} == $child_id
	&& $reply->{post_id} == $POST_ID
	&& $reply->{author} eq 'tast'
	&& $reply->{text} eq 'tust'
	&& $reply->{timestamp} > 0,
	'add reply')
;

ok(
	$db->comments($POST_ID)->[0]{replies}[0]{id} == $child_id,
	'check reply'
);

$db->delete_comment($id);

ok(
	scalar @{ $db->comments($POST_ID) } == 0,
	'delete comment'
);

1;