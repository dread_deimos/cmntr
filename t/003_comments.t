use Test::More tests => 7;

use lib 'lib';
use strict;
use warnings;

use cmntr;
use Dancer::Test;
use JSON;

route_exists [GET => '/comments'], 'a route handler is defined for /comments';
response_status_is [GET => '/comments'], 200, 'response status is 200 for /';

{
	route_exists [POST => '/comment'], 'a route handler is defined for POST /comment';
	my $response = dancer_response (
		POST => '/comment',
		{ params => { author => 'test', text => 'test comment', post_id => 1 } },
	);
	is $response->{status}, 200, 'response status is 200 for POST /comment';
	my $data = from_json $response->{content};
	is $data->{status}, 1, 'created first comment';
}

route_exists [DELETE => '/comment'], 'a route handler is defined for DELETE /comment';
response_status_is [DELETE => '/comment'], 200, 'response status is 200 for DELETE /comment';

1;