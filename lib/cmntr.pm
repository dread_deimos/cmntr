package cmntr;

use strict;
use warnings;

use Dancer ':syntax';
use Database;
use HTML::Entities;

our $VERSION = '0.1';

our $db = Database->new(file => config->{database});

our %validation = (
	add_comment => {
		author => sub {
			my ($value) = @_;

			if (length $value > 100) {
				return 'Too long author name';
			}
			elsif (length $value == 0) {
				return 'Author name is empty';
			}

			return;
		},
		text => sub {
			my ($value) = @_;

			if (length $value > 2000) {
				return 'Too long comment text';
			}
			elsif (length $value == 0) {
				return 'Author name is empty';
			}

			return;
		},
		parent => sub {
			my ($value) = @_;

			if ($value && $value !~ /^\d+$/) {
				return 'Parent comment ID must be a number';
			}

			return;
		},
		post_id => sub {
			my ($value) = @_;

			if (!$value) {
				return 'Post ID not specified';
			}
			elsif ($value !~ /^\d+$/) {
				return 'Post ID must be a number';
			}

			return;
		},
	},
);

get '/' => sub {
    template 'index';
};

post '/comment' => sub {
	my %response = (
		status => 0,
	);

	my %errors;
	foreach my $field (keys %{ $validation{add_comment} }) {
		my $error = &{ $validation{add_comment}{$field} }(param $field);
		$errors{$field} = $error if $error;
	}

	if (! keys %errors) {
		my ($post_id, $author, $text, $parent) = @{ params() }{qw/post_id author text parent/};

		# Escape HTML special characters
		$author  = encode_entities(param 'author');
		$text    = encode_entities(param 'text');

		$response{status} = $db->add_comment($post_id, $author, $text, $parent) && 1;
		$db->save if $response{status};
	} else {
		$response{errors} = \%errors;
	}

	return to_json \%response;
};

del '/comment' => sub {
	my $status = $db->delete_comment(param 'id');
	$db->save if $status;
	return to_json {
		status => $status,
	};
};

get '/comments' => sub {
	return to_json $db->comments(param 'post_id');
};

1;
