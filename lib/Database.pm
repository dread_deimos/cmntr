package Database;

use Storable qw(nstore retrieve);
use POSIX qw(strftime);

use strict;
use warnings;

sub new {
	my ($class, %options) = @_;

	my $self = {
		_data => {
			comments       => [],
			comments_count => 0,
		},
		_file => $options{file},
	};

	if ($self->{_file} && -f $self->{_file}) {
		$self->{_data} = retrieve $self->{_file};
	}

	return bless $self, $class;
}

sub save {
	my ($self) = @_;
	if ($self->{_file}) {
		nstore $self->{_data}, $self->{_file};
	}
	return;
}

sub comments {
	my ($self, $post_id, $parent_id) = @_;

	$post_id   //= 0;
	$parent_id //= 0;

	my @result;

	foreach my $comment ( $self->_get_children_comments($post_id, $parent_id) ) {
		my %item = %$comment;
		$item{timestamp} = strftime '%Y-%m-%d %H:%M:%S', localtime($item{timestamp});
		$item{replies} = $self->comments($post_id, $item{id});
		push @result, \%item;
	}

	return \@result;
}

sub add_comment {
	my ($self, $post_id, $author, $text, $parent_id) = @_;
	my $id = ++$self->{_data}{comments_count};

	my %item = (
		id        => $id,
		post_id   => $post_id,
		author    => $author,
		text      => $text,
		timestamp => time,
		parent    => 0,
	);

	if (my $parent = $self->get_comment($parent_id)) {
		$item{parent} = $parent->{id};
	}

	push @{ $self->{_data}{comments} }, \%item;

	return $id;
}

sub delete_comment {
	my ($self, $id) = @_;

	my $comment = $self->get_comment($id);

	return 0 unless $comment;

	foreach my $child_id ($self->_get_children_comments($id)) {
		$self->delete_comment($child_id);
	}

	@{ $self->{_data}{comments} } = grep { $_->{id} != $id } @{ $self->{_data}{comments} };

	return 1;
}

sub get_comment {
	my ($self, $id) = @_;

	return unless $id;

	my ($comment) = grep { ref $_ && $_->{id} == $id } @{ $self->{_data}{comments} };

	return $comment;
}

sub _get_children_comments {
	my ($self, $post_id, $id) = @_;
	$post_id //= 0;
	$id      //= 0;
	return grep { $_->{post_id} == $post_id && $_->{parent} == $id } @{ $self->{_data}{comments} };
}

1;